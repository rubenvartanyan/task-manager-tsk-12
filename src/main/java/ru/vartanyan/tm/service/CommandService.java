package ru.vartanyan.tm.service;

import ru.vartanyan.tm.api.ICommandRepository;
import ru.vartanyan.tm.api.ICommandService;
import ru.vartanyan.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }
}
