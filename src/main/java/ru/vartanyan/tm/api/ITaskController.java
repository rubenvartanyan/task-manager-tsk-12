package ru.vartanyan.tm.api;

public interface ITaskController {

    void showList();

    void showTaskByIndex();

    void showTaskByName();

    void showTaskById();

    void removeTaskByIndex();

    void removeTaskById();

    void removeTaskByName();

    void updateTaskByIndex();

    void updateTaskById();

    void create();

    void clear();

    void startTaskById();

    void startTaskByName();

    void startTaskByIndex();

    void finishTaskById();

    void finishTaskByName();

    void finishTaskByIndex();

    void updateTaskStatusById();

    void updateTaskStatusByName();

    void updateTaskStatusByIndex();

}
