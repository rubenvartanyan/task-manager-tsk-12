package ru.vartanyan.tm.api;

import ru.vartanyan.tm.enumerated.Status;
import ru.vartanyan.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task add(Task task);

    Task remove(Task task);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    void clear();

    Task add(String name, String description);

    Task updateTaskById(String id, String name, String description);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task startTaskById(String id);

    Task startTaskByName(String name);

    Task startTaskByIndex(Integer index);

    Task finishTaskById(String id);

    Task finishTaskByName(String name);

    Task finishTaskByIndex(Integer index);

    Task updateTaskStatusById(String id, Status status);

    Task updateTaskStatusByName(String name, Status status);

    Task updateTaskStatusByIndex(Integer index, Status status);

}
