package ru.vartanyan.tm.api;

import ru.vartanyan.tm.model.Project;

import java.util.List;

public interface IProjectRepository {
    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project add(Project project);

    Project remove(Project project);

    void clear();
}
