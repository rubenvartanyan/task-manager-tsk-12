package ru.vartanyan.tm.repository;

import ru.vartanyan.tm.api.ITaskRepository;
import ru.vartanyan.tm.api.ITaskService;
import ru.vartanyan.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public Task findOneById(final String id) {
        for (Task task: list) {
            if (task.getId() == id) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return list.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (Task task: list) {
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task add(final Task task) {
        list.add(task);
        return task;
    }

    @Override
    public Task remove(final Task task) {
        list.remove(task);
        return task;
    }

    @Override
    public void clear() {
        list.clear();
    }

}
